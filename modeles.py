import base64
import json
import sys

class BotInfo:
    def __init__(self, name, picture):
        self.name = name
        self.picture = picture

    def __str__(self):
        return ('%s, %s' % (self.name, self.picture))

def get_first_infos(json):
    with open(json['picture'], "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return BotInfo(json['name'],encoded_string.decode('utf-8'))  

def get_all_infos(json):
    with open(json['picture'], "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    infos = BotInfo(json['name'],encoded_string.decode('utf-8'))   
    infos.information = json['infos']
    infos.commands = json['commands']

    return infos