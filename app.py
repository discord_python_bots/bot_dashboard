from flask import Flask
from flask import jsonify, Response
import json
import sys

import helper

app = Flask(__name__)

@app.route("/projects", methods=['GET'])
def projects():
    infos_bots = list()
    bots = helper.list_bots()
    for bot in bots:
        infos = helper.get_infos(bot)
        infos_bots.append(infos)

    json_infos_bot = json.dumps([ob.__dict__ for ob in infos_bots], sort_keys=True, indent=4)
    return Response(json_infos_bot,  mimetype='application/json')
    
@app.route("/project/<name>", methods=['GET'])
def project(name):
    if not helper.is_bot_exist(name):
        return Response(json.dumps({'error': 'bot doesn\'t exist'}), mimetype='application/json')
    
    return Response(json.dumps(helper.get_all_infos(name).__dict__, sort_keys=True, indent=4), mimetype='application/json')
    